{
  "data_set_description": {
    "collection_date": "30-09-2022",
    "creator": "Ihsan Ullah",
    "default_target_attribute": "CATEGORY",
    "description": "## **Meta-Album Boats Dataset (Mini)**\n***\nThe original version of the Meta-Album boats dataset is called MARVEL dataset (https://github.com/avaapm/marveldataset2016). It has more than 138 000 images of 26 different maritime vessels in their natural background. Each class can have 1 802 to 8 930 images of variable resolutions. To preprocess this dataset, we either duplicate the top and bottom-most 3 rows or the left and right most 3 columns based on the orientation of the original image to create square images. No cropping was applied because the boats occupy most of the image, and applying this technique will lead to incomplete images. Finally, the square images were resized into 128x128 px using an anti-aliasing filter  \n\n\n\n### **Dataset Details**\n![](https://meta-album.github.io/assets/img/samples/BTS.png)\n\n**Meta Album ID**: VCL.BTS  \n**Meta Album URL**: [https://meta-album.github.io/datasets/BTS.html](https://meta-album.github.io/datasets/BTS.html)  \n**Domain ID**: VCL  \n**Domain Name**: Vehicles  \n**Dataset ID**: BTS  \n**Dataset Name**: Boats  \n**Short Description**: Dataset with images of different boats  \n**\\# Classes**: 26  \n**\\# Images**: 1040  \n**Keywords**: vehicles, boats  \n**Data Format**: images  \n**Image size**: 128x128  \n\n**License (original data release)**: Cite paper to use dataset  \n**License (Meta-Album data release)**: CC BY-NC 4.0  \n**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by-nc/4.0/](https://creativecommons.org/licenses/by-nc/4.0/)  \n\n**Source**: MARVEL: A LARGE-SCALE IMAGE DATASET FOR MARITIME VESSELS  \n**Source URL**: https://github.com/avaapm/marveldataset2016  \n  \n**Original Author**: Gundogdu E., Solmaz B, Yucesoy V., Koc A.  \n**Original contact**:   \n\n**Meta Album author**: Dustin Carrion  \n**Created Date**: 01 March 2022  \n**Contact Name**: Ihsan Ullah  \n**Contact Email**: meta-album@chalearn.org  \n**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  \n\n\n\n### **Cite this dataset**\n```\n@InProceedings{MARVEL,\n    author=\"Gundogdu, Erhan and Solmaz, Berkan and Yucesoy, Veysel and Koc, Aykut\",\n    editor=\"Lai, Shang-Hong and Lepetit, Vincent and Nishino, Ko and Sato, Yoichi\",\n    title=\"MARVEL: A Large-Scale Image Dataset for Maritime Vessels\",\n    booktitle=\"Computer Vision --  ACCV 2016\",\n    year=\"2017\",\n    publisher=\"Springer International Publishing\",\n    address=\"Cham\",\n    pages=\"165--180\",\n    isbn=\"978-3-319-54193-8\"\n}\n```\n\n\n### **Cite Meta-Album**\n```\n@inproceedings{meta-album-2022,\n        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},\n        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},\n        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},\n        url = {https://meta-album.github.io/},\n        year = {2022}\n    }\n```\n\n\n### **More**\nFor more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  \nFor details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  \nSupporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  \nMeta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  \n\n\n\n### **Other versions of this dataset**\n[[Micro]](https://www.openml.org/d/44279)  [[Extended]](https://www.openml.org/d/44343)",
    "description_version": "2",
    "file_id": "22111009",
    "format": "arff",
    "id": "44309",
    "language": "English",
    "licence": "CC BY-NC 4.0",
    "md5_checksum": "fbafb4843bd09460173712eb9474b4c2",
    "minio_url": "https://data.openml.org/datasets/0004/44309/dataset_44309.pq",
    "name": "Meta_Album_BTS_Mini",
    "parquet_url": "https://data.openml.org/datasets/0004/44309/dataset_44309.pq",
    "processing_date": "2022-10-28 16:29:05",
    "status": "active",
    "upload_date": "2022-10-28T16:28:46",
    "url": "https://api.openml.org/data/v1/download/22111009/Meta_Album_BTS_Mini.arff",
    "version": "1",
    "visibility": "public"
  }
}